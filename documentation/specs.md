## Fonctionnalités :

1. Stocker des livres
1. Les restituer
    - par ordre alphabétique de titre
    - par ordre alphabétique d'auteurs
1. Faire des recherches
    - sur le titre
    - sur l'auteur

