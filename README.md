# Mon super projet (distant)

## Auteur

- Nom : Tabouda
- Prénom : Ramdane
- Métier : Développeur web Fullstack

## Description

Application de recherche de livre 
Lister des livres des auteurs

## Installation

Lancer le script "install.sh"

## Désinstallation

Lancer le script **uninstall.sh**

